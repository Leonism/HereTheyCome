﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

	public AudioClip mainTheme;
	public AudioClip menuTheme;

	string sceneName;

	void Start() {
		OnLevelWasLoaded(0);
	}


	//Unity might call this method automatically
	void OnLevelWasLoaded(int sceneIndex) {
		string newSceneName = SceneManager.GetActiveScene().name;
		if (newSceneName != sceneName) {
			sceneName = newSceneName;
			//Avoids calling the method from the audio manager duplicate.
			Invoke("PlayMusic", .2f);
		}
	}

	void PlayMusic() {
		AudioClip clipToPlay = null;

		if (sceneName == "Menu") {
			clipToPlay = menuTheme;
		} else if (sceneName == "Game") {
			clipToPlay = mainTheme;
		}

		if (clipToPlay != null) {
			AudioManager.instance.PlayMusic(clipToPlay, 2);
			Invoke("PlayMusic", clipToPlay.length);
		}

	}

}