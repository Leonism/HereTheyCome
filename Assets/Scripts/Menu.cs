﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public GameObject mainMenuHolder;
	public GameObject optionsMenuHolder;
	public GameObject HowToHolder;
	public GameObject lvlSelectorHolder;

	public Text highScoreUI;
	public GameObject[] lockedLVLs;
	public Button[] lvlButtons;
	public Slider[] volumeSliders;
	public Toggle[] resolutionToggles;
	public Toggle fullscreenToggle;
	public int[] screenWidths;
	int activeScreenResIndex;

	void Start() {

		int highScore = PlayerPrefs.GetInt("High Score", 0);
		highScoreUI.text = highScore.ToString();

		activeScreenResIndex = PlayerPrefs.GetInt("screen res index");
		bool isFullscreen = (PlayerPrefs.GetInt("fullscreen") == 1) ? true : false;

		volumeSliders[0].value = AudioManager.instance.masterVolumePercent;
		volumeSliders[1].value = AudioManager.instance.musicVolumePercent;
		volumeSliders[2].value = AudioManager.instance.sfxVolumePercent;

		for (int i = 0; i < resolutionToggles.Length; i++) {
			resolutionToggles[i].isOn = i == activeScreenResIndex;
		}

		fullscreenToggle.isOn = isFullscreen;
	}

	void Update() {

		//Delete all saved settings. Useful for debugging or resetting playerprefs

		if (Input.GetKeyUp("delete")) {
			PlayerPrefs.DeleteAll();

			highScoreUI.text = "0";

			//Lock all levels from 2 and above, or they will stay unlocked.
			for (int i = 0; i <= 3; i++) {
				lockedLVLs[i].SetActive(true);
				lvlButtons[i].interactable = false;
			}

		}
	}

	public void Play() {

		mainMenuHolder.SetActive(false);

		//Load the progress of the player and show unlocked levels 

		int lvlReached = PlayerPrefs.GetInt("Level Reached", 1);

		for (int i = 0; i <= lvlReached - 2; i++) {
			lockedLVLs[i].SetActive(false);
			lvlButtons[i].interactable = true;
		}

		lvlSelectorHolder.SetActive(true);
	}

	public void Quit() {
		Application.Quit();
	}

	public void OptionsMenu() {
		mainMenuHolder.SetActive(false);
		optionsMenuHolder.SetActive(true);
	}

	public void HowToPlay() {
		mainMenuHolder.SetActive(false);
		HowToHolder.SetActive(true);
	}

	public void MainMenu() {
		mainMenuHolder.SetActive(true);
		HowToHolder.SetActive(false);
		optionsMenuHolder.SetActive(false);
		lvlSelectorHolder.SetActive(false);
	}

	public void StartLevel(int lvl) {

		//Use static class to send the selected level to the spawner in the next scene.
		StaticClass.lvlReached = lvl;

		SceneManager.LoadScene("Game");
	}

	public void SetScreenResolution(int i) {
		if (resolutionToggles[i].isOn) {
			activeScreenResIndex = i;
			float aspectRatio = 16 / 9f;
			Screen.SetResolution(screenWidths[i], (int)(screenWidths[i] / aspectRatio), false);
			PlayerPrefs.SetInt("screen res index", activeScreenResIndex);
			PlayerPrefs.Save();
		}
	}

	public void SetFullscreen(bool isFullscreen) {
		for (int i = 0; i < resolutionToggles.Length; i++) {
			resolutionToggles[i].interactable = !isFullscreen;
		}

		if (isFullscreen) {

			//The following code stopped working after updating unity
			/*Resolution[] allResolutions = Screen.resolutions;
			Resolution maxResolution = allResolutions[allResolutions.Length - 1];

			Screen.SetResolution(maxResolution.width, maxResolution.height, true);*/

			Screen.SetResolution(Display.main.systemWidth, Display.main.systemHeight, true);

		} else {
			SetScreenResolution(activeScreenResIndex);
		}

		PlayerPrefs.SetInt("fullscreen", ((isFullscreen) ? 1 : 0));
		PlayerPrefs.Save();
	}

	public void SetMasterVolume(float value) {
		AudioManager.instance.SetVolume(value, AudioManager.AudioChannel.Master);
	}

	public void SetMusicVolume(float value) {
		AudioManager.instance.SetVolume(value, AudioManager.AudioChannel.Music);
	}

	public void SetSfxVolume(float value) {
		AudioManager.instance.SetVolume(value, AudioManager.AudioChannel.Sfx);
	}

}