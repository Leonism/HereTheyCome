﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {
    
    //1-Shield/DMG immunity
    //2-Fire/a kill with 1 shot
    protected int powerUpType;

    //Every Power up has a duration for balancing puposes
    protected float duration;

    LivingEntity playerEntity;

    // Start is called once
    protected virtual void Start() {
        playerEntity = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnTriggerEnter(Collider other) {

        if (other.name == "Player") {

            //if (playerEntity != null)
            playerEntity.changeMode(duration, powerUpType);
            GameObject.Destroy(gameObject);
        }
    }
}
