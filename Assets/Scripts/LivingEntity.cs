﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices.WindowsRuntime;

public class LivingEntity : MonoBehaviour, IDamageable {

	public float startingHealth;
	public float health { get; protected set; }
	protected bool dead;

	public enum Mode { Normal = 0, Shield = 1, Fire = 2, Spear = 3 };
	public Mode currentMode;

	public Animator animator;

	public event System.Action OnDeath;

	protected virtual void Start() {

		animator = gameObject.GetComponent<Animator>();

		currentMode = Mode.Normal;

		health = startingHealth;
	}

	public virtual void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection) {
		// Do some stuff here with hit var
		TakeDamage(damage);
	}

	public virtual void TakeDamage(float damage) {

		if (currentMode != Mode.Shield) {
			health -= damage;

			if (health <= 0 && !dead) {
				Die();
			}
		}
	}

	[ContextMenu("Self Destruct")]
	public virtual void Die() {
		dead = true;

		if (OnDeath != null) {
			OnDeath();
		}

		/*if (animator == null)
			GameObject.Destroy(gameObject);*/

		//Destroy object as animation event in the method DestroyObject
		GameObject.Destroy(gameObject);

	}

	public void changeMode(float powerUpDuration, int newMode) {

		if (currentMode == Mode.Normal) {
			StartCoroutine(enableMode(powerUpDuration, newMode));
		} else {
			StopCoroutine("enableMode");
			currentMode = Mode.Normal;
			StartCoroutine(enableMode(powerUpDuration, newMode));
		}
	}

	IEnumerator enableMode(float duration, int newMode) {

		if (currentMode == Mode.Normal) {
			currentMode = (Mode)newMode;
			yield return new WaitForSeconds(duration);
			//print(currentMode.ToString());
			currentMode = Mode.Normal;
		}


	}

	/*int getMode() {
		return currentMode;
	}*/
}
