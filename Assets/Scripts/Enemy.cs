﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.UIElements;

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
public class Enemy : LivingEntity {

	public enum State { Idle, Chasing, Attacking };
	State currentState;

	public ParticleSystem deathEffect;
	public static event System.Action OnDeathStatic;

	UnityEngine.AI.NavMeshAgent pathfinder;
	Transform target;
	LivingEntity targetEntity;
	Material skinMaterial;

	Color originalColour;

	float attackDistanceThreshold = .5f;
	float timeBetweenAttacks = 1;
	float damage = 1;

	GameObject dmgIndicator;
	public UnityEngine.UI.Image dmgIndicatorImage;
	bool indicatorOn = false;

	float nextAttackTime;
	float myCollisionRadius;
	float targetCollisionRadius;

	bool hasTarget;

	void Awake() {
		pathfinder = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();

		if (GameObject.FindGameObjectWithTag("Player") != null) {
			hasTarget = true;

			target = GameObject.FindGameObjectWithTag("Player").transform;
			targetEntity = target.GetComponent<LivingEntity>();

			myCollisionRadius = GetComponent<CapsuleCollider>().radius;
			targetCollisionRadius = target.GetComponent<CapsuleCollider>().radius;
		}

		if ((dmgIndicator = GameObject.FindGameObjectWithTag("Damage Indicator")) != null) {
			dmgIndicatorImage = dmgIndicator.GetComponent<UnityEngine.UI.Image>();
			
		}

	}

	protected override void Start() {
		base.Start();

		if (hasTarget) {
			//currentState = State.Chasing;
			setCurrentState(State.Chasing);

			targetEntity.OnDeath += OnTargetDeath;

			StartCoroutine(UpdatePath());
		}

	}

	public void SetCharacteristics(float moveSpeed, int hitsToKillPlayer, float enemyHealth, Color skinColour) {
		
		pathfinder.speed = moveSpeed;

		if (hasTarget) {
			damage = Mathf.Ceil(targetEntity.startingHealth / hitsToKillPlayer);
		}
		startingHealth = enemyHealth;

		//deathEffect.startColor = new Color(skinColour.r, skinColour.g, skinColour.b, 1);

		ParticleSystem.MainModule main = deathEffect.main;
		main.startColor = new Color(skinColour.r, skinColour.g, skinColour.b, 1);

		skinMaterial = GetComponent<Renderer>().material;
		skinMaterial.color = skinColour;
		originalColour = skinMaterial.color;
	}

	public override void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection) {
		AudioManager.instance.PlaySound("Impact", transform.position);
		if (damage >= health && !dead) {

			hasTarget = false;

			if (OnDeathStatic != null) {
				//animator.SetBool("isDead", true);
				OnDeathStatic();
			}
			
			AudioManager.instance.PlaySound("Enemy Death", transform.position);

			ParticleSystem.MainModule main = deathEffect.main;

			Destroy(Instantiate(deathEffect.gameObject, hitPoint, Quaternion.FromToRotation(Vector3.forward, hitDirection)) as GameObject, main.startLifetime.constant);
		}

		base.TakeHit(damage, hitPoint, hitDirection);
	}

	void OnTargetDeath() {
		hasTarget = false;
		currentState = State.Idle;
		//setCurrentState(State.Idle);
	}

	void Update() {

		if (hasTarget) {
			if (Time.time > nextAttackTime) {
				float sqrDstToTarget = (target.position - transform.position).sqrMagnitude;
				if (sqrDstToTarget < Mathf.Pow(attackDistanceThreshold + myCollisionRadius + targetCollisionRadius, 2)) {
					nextAttackTime = Time.time + timeBetweenAttacks;
					AudioManager.instance.PlaySound("Enemy Attack", transform.position);
					StartCoroutine(Attack());
				}

			}
		}

	}

	IEnumerator Attack() {

		//currentState = State.Attacking;
		setCurrentState(State.Attacking);
		pathfinder.enabled = false;

		Vector3 originalPosition = transform.position;
		Vector3 dirToTarget = (target.position - transform.position).normalized;
		Vector3 attackPosition = target.position - dirToTarget * (myCollisionRadius);

		float attackSpeed = 3;
		float percent = 0;

		skinMaterial.color = Color.red;
		bool hasAppliedDamage = false;

		while (percent <= 1) {

			if (percent >= .5f && !hasAppliedDamage) {
				hasAppliedDamage = true;
				DealDamage();
			}

			percent += Time.deltaTime * attackSpeed;
			float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
			transform.position = Vector3.Lerp(originalPosition, attackPosition, interpolation);

			yield return null;
		}

		skinMaterial.color = originalColour;
		setCurrentState(State.Chasing);
		//currentState = State.Chasing;
		//animator.SetInteger("currentState", 1);
		pathfinder.enabled = true;
	}

	void DealDamage()
	{
		
		targetEntity.TakeDamage(damage);

		//Flash an indicator of applying dmg
		StartCoroutine(ShowDamageIndicator());

	}

	IEnumerator ShowDamageIndicator() {

		indicatorOn = true;

		float fadeSpeed = 4;
		float percent = 0;
		Color tempColor = dmgIndicatorImage.color;

		while (percent <= 1) {

			percent += Time.deltaTime * fadeSpeed;
			float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;

			tempColor.a = interpolation;
			dmgIndicatorImage.color = tempColor;

			yield return null;
		}

		indicatorOn = false;

	}


	IEnumerator UpdatePath() {
		float refreshRate = .25f;

		while (hasTarget) {
			if (currentState == State.Chasing) {
				Vector3 dirToTarget = (target.position - transform.position).normalized;
				Vector3 targetPosition = target.position - dirToTarget * (myCollisionRadius + targetCollisionRadius + attackDistanceThreshold / 2);
				if (!dead) {
					pathfinder.SetDestination(targetPosition);
				}
			}
			yield return new WaitForSeconds(refreshRate);
		}
	}

	//Set currentstate and adjust animator
	void setCurrentState(State newState) {

		currentState = newState;
		animator.SetInteger("currentState", (int) newState);
	}
}