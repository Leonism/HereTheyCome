﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : PowerUp
{
    //A kill with one shot
    //2 options for balancnig, the next n shots will kill when hit or the shots in the next n seconds
    protected override void Start() {
        base.Start();
        duration = 3f;
        powerUpType = 2;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
