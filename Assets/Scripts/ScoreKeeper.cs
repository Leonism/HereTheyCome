﻿using UnityEngine;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

	public static int score { get; private set; }
	float lastEnemyKillTime;
	int streakCount;
	float streakExpiryTime = 1;

	void Start() {
		
		//Unsubscribe
		Enemy.OnDeathStatic += OnEnemyKilled;

		//Unsubscribe to the event or it will provoke twice after hitting play again
		FindObjectOfType<Player>().OnDeath += OnPlayerDeath;
	}

	void OnEnemyKilled() {

		if (Time.time < lastEnemyKillTime + streakExpiryTime) {
			streakCount++;
		} else {
			streakCount = 0;
		}

		lastEnemyKillTime = Time.time;

		score += 3 + 2 * streakCount;
	}

	void OnPlayerDeath() {

		//Unsubscribe or it will proc twice if the player plays again, because static event still exist
		//FindObjectOfType<Player>().OnDeath -= OnPlayerDeath;
		Enemy.OnDeathStatic -= OnEnemyKilled;
		

		//Save the score if its a high score
		int highScore = PlayerPrefs.GetInt("High Score", 0);

		if (score > highScore) {

			PlayerPrefs.SetInt("High Score", score);
			PlayerPrefs.Save();
		}

		score = 0;
	}

}