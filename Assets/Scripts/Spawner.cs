﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public bool devMode;

	public Wave[] waves;
	public Enemy enemy;

	LivingEntity playerEntity;
	Transform playerT;

	Wave currentWave;
	int currentWaveNumber;
	

	int enemiesRemainingToSpawn;
	int enemiesRemainingAlive;
	float nextSpawnTime;

	MapGenerator map;

	public PowerUp[] powerUps;
	public float timeBetweenPowerUpSpawns;
	float nextPowerUpSpawnTime;

	float timeBetweenCampingChecks = 2;
	float campThresholdDistance = 1.5f;
	float nextCampCheckTime;
	Vector3 campPositionOld;
	bool isCamping;

	bool isDisabled;

	public event System.Action<int> OnNewWave;

	void Start() {
		playerEntity = FindObjectOfType<Player>();
		playerT = playerEntity.transform;

		nextCampCheckTime = timeBetweenCampingChecks + Time.time;
		campPositionOld = playerT.position;
		playerEntity.OnDeath += OnPlayerDeath;

		map = FindObjectOfType<MapGenerator>();

		//Start at the level that the player selected. Set in menu class.
		currentWaveNumber = StaticClass.lvlReached;

		NextWave();
		
	}

	void Update() {
		if (!isDisabled) {
			if (Time.time > nextCampCheckTime) {
				nextCampCheckTime = Time.time + timeBetweenCampingChecks;

				isCamping = (Vector3.Distance(playerT.position, campPositionOld) < campThresholdDistance);
				campPositionOld = playerT.position;
			}

			if ((enemiesRemainingToSpawn > 0 || currentWave.infinite) && Time.time > nextSpawnTime) {
				enemiesRemainingToSpawn--;
				nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;

				StartCoroutine("SpawnEnemy");
			}
		}

		if (devMode) {
			if (Input.GetKeyDown(KeyCode.Return)) {
				StopCoroutine("SpawnEnemy");
				foreach (Enemy enemy in FindObjectsOfType<Enemy>()) {
					GameObject.Destroy(enemy.gameObject);
				}

				removePowerUps();
				StopCoroutine("SpawnPowerUp");

				NextWave();
			}
		}


	}

	IEnumerator SpawnEnemy() {

		float spawnDelay = 1;
		float tileFlashSpeed = 4;

		Transform spawnTile = map.GetRandomOpenTile();
		if (isCamping) {
			spawnTile = map.GetTileFromPosition(playerT.position);
		}
		Material tileMat = spawnTile.GetComponent<Renderer>().material;
		Color initialColour = Color.white;
		Color flashColour = Color.red;
		float spawnTimer = 0;

		while (spawnTimer < spawnDelay) {

			tileMat.color = Color.Lerp(initialColour, flashColour, Mathf.PingPong(spawnTimer * tileFlashSpeed, 1));

			spawnTimer += Time.deltaTime;
			yield return null;
		}

		Enemy spawnedEnemy = Instantiate(enemy, spawnTile.position + Vector3.up, Quaternion.identity) as Enemy;
		spawnedEnemy.OnDeath += OnEnemyDeath;
		spawnedEnemy.SetCharacteristics(currentWave.moveSpeed, currentWave.hitsToKillPlayer, currentWave.enemyHealth, currentWave.skinColour);
	}

	IEnumerator SpawnPowerUp() {

		//Transform spawnTile;
		int randomPowerup;

		System.Random prng = new System.Random(1);
		nextPowerUpSpawnTime = Time.time + timeBetweenPowerUpSpawns;	

		while (!isDisabled) {

			if (Time.time > nextPowerUpSpawnTime) {
				Transform spawnTile = map.GetRandomOpenTile();

				randomPowerup = prng.Next(0, powerUps.Length);
				//PowerUp powerUp = Instantiate(powerUps[randomPowerup], spawnTile.position + Vector3.up, Quaternion.identity) as PowerUp;
				PowerUp powerUp = Instantiate(powerUps[randomPowerup], spawnTile.position + Vector3.up * 0.5f, (powerUps[randomPowerup].transform.rotation)) as PowerUp;
				nextPowerUpSpawnTime += timeBetweenPowerUpSpawns;
			}

			
			yield return null;
		}

	}

	void OnPlayerDeath() {
		isDisabled = true;
	}

	void OnEnemyDeath() {
		enemiesRemainingAlive--;

		if (enemiesRemainingAlive == 0) {
			removePowerUps();
			StopCoroutine("SpawnPowerUp");
			NextWave();
		}
	}

	void ResetPlayerPosition() {
		playerT.position = map.GetTileFromPosition(Vector3.zero).position + Vector3.up * 3.5f;
	}

	void NextWave() {

		StartCoroutine("SpawnPowerUp");

		if (currentWaveNumber > 0) {
			AudioManager.instance.PlaySound2D("Level Complete");
		}
		currentWaveNumber++;

		//Save the progress of the player, make sure thats the higher lvl that is reached or else it will set the current one as the highest
		int lvlSaved = PlayerPrefs.GetInt("Level Reached");
		if (lvlSaved < currentWaveNumber) {
			PlayerPrefs.SetInt("Level Reached", currentWaveNumber);
			PlayerPrefs.Save();
		}


		if (currentWaveNumber - 1 < waves.Length) {
			currentWave = waves[currentWaveNumber - 1];

			enemiesRemainingToSpawn = currentWave.enemyCount;
			enemiesRemainingAlive = enemiesRemainingToSpawn;

			if (OnNewWave != null) {
				OnNewWave(currentWaveNumber);
			}
			ResetPlayerPosition();
		}
	}

	[System.Serializable]
	public class Wave {
		public bool infinite;
		public int enemyCount;
		public float timeBetweenSpawns;

		public float moveSpeed;
		public int hitsToKillPlayer;
		public float enemyHealth;
		public Color skinColour;
	}

	void removePowerUps() {

		foreach (PowerUp powerUp in FindObjectsOfType<PowerUp>()) {
			GameObject.Destroy(powerUp.gameObject);
		}
	}

}