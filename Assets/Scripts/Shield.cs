﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : PowerUp
{
    // Start is called before the first frame update
    protected override void Start() {

        base.Start();
        duration = 3f;
        powerUpType = 1;
    }
}
