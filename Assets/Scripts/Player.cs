﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(GunController))]
public class Player : LivingEntity {

	public enum State { Idle, WalkingForward, WalkingBackward, WalkingLeft, WalkingRight };

	public float moveSpeed = 5;

	public Crosshairs crosshairs;

	Camera viewCamera;
	PlayerController controller;
	GunController gunController;

	protected override void Start() {
		base.Start();
	}

	void Awake() {
		controller = GetComponent<PlayerController>();
		gunController = GetComponent<GunController>();
		viewCamera = Camera.main;
		FindObjectOfType<Spawner>().OnNewWave += OnNewWave;
	}

	void OnNewWave(int waveNumber) {
		health = startingHealth;
		gunController.EquipGun(waveNumber - 1);
	}

	void Update() {
		// Movement input

		//int hor = Input.GetAxisRaw("Horizontal");

		float ver = Input.GetAxisRaw("Vertical");
		float hor = Input.GetAxisRaw("Horizontal");

		Vector3 moveInput = new Vector3(hor, 0, ver);	

		Vector3 moveVelocity = moveInput.normalized * moveSpeed;
		controller.Move(moveVelocity);

		// Look input
		Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);

		//Plane groundPlane = new Plane(Vector3.up, Vector3.up * gunController.GunHeight);

		Plane groundPlane = new Plane(Vector3.up, Vector3.up * 0.2f);

		float rayDistance;
		Vector3 point;

		if (groundPlane.Raycast(ray, out rayDistance)) {
			point = ray.GetPoint(rayDistance);
			//Debug.DrawLine(ray.origin,point,Color.red);
			controller.LookAt(point);
			crosshairs.transform.position = point;
			crosshairs.DetectTargets(ray);
			if ((new Vector2(point.x, point.z) - new Vector2(transform.position.x, transform.position.z)).sqrMagnitude > 1) {
				gunController.Aim(point);
			}

			//Handle walking animation

			if (ver == 0 && hor == 0)
				ChangeCurrentState(State.Idle);
			else {

				float x = point.x - transform.position.x;
				float z = point.z - transform.position.z;

				if (Mathf.Abs(x) > Mathf.Abs(z)) {

					//player is walking towards the mouse. equal to if x < 0 && ver < 0 || x > 0 && ver > 0
					if (x * hor > 0)
						ChangeCurrentState(State.WalkingForward);
					else if (x * hor < 0)
						ChangeCurrentState(State.WalkingBackward);
					else if (x * ver > 0)
						ChangeCurrentState(State.WalkingLeft);
					else if (x * ver < 0)
						ChangeCurrentState(State.WalkingRight);
				} else {

					if (z * ver > 0)
						ChangeCurrentState(State.WalkingForward);
					else if (z * ver < 0)
						ChangeCurrentState(State.WalkingBackward);
					else if (z * hor < 0)
						ChangeCurrentState(State.WalkingLeft);
					else if (z * hor > 0)
						ChangeCurrentState(State.WalkingRight);
				}
			}
		}

		

		// Weapon input
		if (Input.GetMouseButton(0)) {
			gunController.OnTriggerHold();
		}
		if (Input.GetMouseButtonUp(0)) {
			gunController.OnTriggerRelease();
		}
		if (Input.GetKeyDown(KeyCode.R)) {
			gunController.Reload();
		}

		if (Input.GetKeyDown(KeyCode.Escape)) {

			Die();

		}

			if (transform.position.y < -10) {
			TakeDamage(health);
		}
	}

	public override void Die() {
		AudioManager.instance.PlaySound("Player Death", transform.position);
		base.Die();
	}

	void ChangeCurrentState(State newState)
	{
		animator.SetInteger("currentState", (int)newState);
	}

}