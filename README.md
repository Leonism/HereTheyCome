# Here They Come
Simple game created with Unity3D and C#.

## Table of contents

* [Introduction](#Introduction)
* [Script files](#script-files)
* [Technologies](#technologies)
* [Opening the project in Unity](#opening-the-project-in-unity)
* [Download game/executable only](#download-game/executable-only)
* [Screenshots](#screenshots)
* [Sources](#sources)


## Introduction
This project helped me practice and discover many aspects of programming and Unity game-engine. I focused on learning adding and editing scenes and implementation rather than spend time on a fabulous UI, HUD or Menu Structure.

## Script files
You can find all [script](https://gitlab.com/Leonism/shoot-em/-/tree/master/Assets/Scripts) files (.cs) [here](https://gitlab.com/Leonism/shoot-em/-/tree/master/Assets/Scripts) in Assets/Scripts.

## Technologies
The game has been developped and updated to work with Unity 2020.3.13f1

Other than the obvious implementations, this project also included practicing the following:
- Ray tracing.
- NavMesh.
- Colliders.
- Random level (tile map) generation with map connectivy and obstacle placements.
- Editor scripting.
- Particle effects.
- Event System.
- Animations.

## Opening the project in Unity
Download the whole project and select the root file (x:\...\shoot 'em) in Unity.

## Download game/executable only
The build and the executable are in the folder ["game"](https://gitlab.com/Leonism/shoot-em/-/tree/master/Game).
You can download the rar file and start "Shoot 'em.exe" to run the game.

## Screenshots

A simple main menu. The goal was to practice UI elements in Unity. This Practice continued in the option menu.<br>
<img src="/source/images/main menu.png"  width="640" height="360">
<br><br>

The option menu contains elements that required implementations in the code in order to function.<br>
Selected options will be saved as PlayerPrefs.<br>
<img src="/source/images/options.png"  width="640" height="360">
<br><br>

Of course just a list of the levels, the player can choose the the desired level to start at.<br>
Choosing the level will load the next scene with the selected level.<br>
Progress will be saved and an unlocked level will stay unlocked. The player unlocks a level by reaching it.<br>
<img src="/source/images/levels menu.png"  width="640" height="360">
<br><br>

Next two screenshots show the difference between the 1st Level and the last one.<br>
The game's difficulty will increase each level.<br>
The red bar at the bottom represents the player's health. At the top you can see the score that has been reached, the highest score will be saved as a PlayerPref.<br>
<img src="/source/images/level 1.png"  width="640" height="360"><br>*Level 1*
<br><br>

<img src="/source/images/level 5.png"  width="640" height="360"><br>*Level 5*
<br><br>

## Sources
- Free models and animations from [mixamo](https://www.mixamo.com/#/).
- Assets from the [unity asset store](https://assetstore.unity.com/) were also used.
